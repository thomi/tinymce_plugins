tablebuttons
================

Add table button controls to TinyMCE.

![Table button control - en](tinymce_plugin_tablebuttons_en.png "Table button control - en")
![Table button control - de](tinymce_plugin_tablebuttons_de.png "Table button control - de")
