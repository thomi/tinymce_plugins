/**
 * plugin.js.js - created on Mar 20, 2015 8:59:42 AM by thomi
 * @copyright		win-soft.ch
 * @author			Thomas Winteler <http://www.win-soft.ch>
 */


tinymce.PluginManager.add('tablebuttons', function(editor, url) {
    
    editor.on('init', function() {
        var cssURL = url + '/css/tablebuttons.css';
        if(document.createStyleSheet){
            document.createStyleSheet(cssURL);
        } else {
            cssLink = editor.dom.create('link', {
                        rel: 'stylesheet',
                        href: cssURL
                      });
            document.getElementsByTagName('head')[0].
                      appendChild(cssLink);
        }
    });    
    
    editor.addButton('tablebuttoncontrol', {
            type: 'menubutton',
            title: 'Edit table',
            icon: 'table',
            menu: [
                {
                    text: 'Paste row before',
                    icon: 'tablebuttonrowbefore',
                    onclick: function() {
                        editor.execCommand('mceTableInsertRowBefore', false, editor);
                    }
                },
                {
                    text: 'Paste row after',
                    icon: 'tablebuttonrowafter',
                    onclick: function() {
                        editor.execCommand('mceTableInsertRowAfter', false, editor);
                    }
                },
                {
                    text: 'Delete row',
                    icon: 'tablebuttonrowdelete',
                    onclick: function() {
                        editor.execCommand('mceTableDeleteRow', false, editor);
                    }
                },
                {
                    text: 'Insert column before',
                    icon: 'tablebuttoncolbefore',
                    onclick: function() {
                        editor.execCommand('mceTableInsertColBefore', false, editor);
                    }
                },
                {
                    text: 'Insert column after',
                    icon: 'tablebuttoncolafter',
                    onclick: function() {
                        editor.execCommand('mceTableInsertColAfter', false, editor);
                    }
                },
                {
                    text: 'Delete column',
                    icon: 'tablebuttoncoldelete',
                    onclick: function() {
                        editor.execCommand('mceTableDeleteCol', false, editor);
                    }
                },
                {
                    text: 'Merge cells',
                    icon: 'tablebuttonmergecells',
                    onclick: function() {
                        editor.execCommand('mceTableMergeCells', false, editor);
                    }
                },
                {
                    text: 'Split cell',
                    icon: 'tablebuttonsplitcell',
                    onclick: function() {
                        editor.execCommand('mceTableSplitCells', false, editor);
                    }
                }
            ]
    });
    
    
    editor.addButton('tablebuttonrowbefore', {
        title: 'Paste row before',
        icon: 'tablebuttonrowbefore',
        onclick: function() {
            editor.execCommand('mceTableInsertRowBefore', false, editor);
        }
    });

    editor.addButton('tablebuttonrowafter', {
        title: 'Paste row after',
        icon: 'tablebuttonrowafter',
        onclick: function() {
            editor.execCommand('mceTableInsertRowAfter', false, editor);
        }
    });

    editor.addButton('tablebuttoncolbefore', {
        title: 'Insert column before',
        icon: 'tablebuttoncolbefore',
        onclick: function() {
            editor.execCommand('mceTableInsertColBefore', false, editor);
        }
    });

    editor.addButton('tablebuttoncolafter', {
        title: 'Insert column after',
        icon: 'tablebuttoncolafter',
        onclick: function() {
            editor.execCommand('mceTableInsertColAfter', false, editor);
        }
    });

    editor.addButton('tablebuttonrowdelete', {
        title: 'Delete row',
        icon: 'tablebuttonrowdelete',
        onclick: function() {
            editor.execCommand('mceTableDeleteRow', false, editor);
        }
    });

    editor.addButton('tablebuttoncoldelete', {
        title: 'Delete column',
        icon: 'tablebuttoncoldelete',
        onclick: function() {
            editor.execCommand('mceTableDeleteCol', false, editor);
        }
    });

    editor.addButton('tablebuttonmergecells', {
        title: 'Merge cells',
        icon: 'tablebuttonmergecells',
        onclick: function() {
            editor.execCommand('mceTableMergeCells', false, editor);
        }
    });

    editor.addButton('tablebuttonsplitcell', {
        title: 'Split cell',
        icon: 'tablebuttonsplitcell',
        onclick: function() {
            editor.execCommand('mceTableSplitCells', false, editor);
        }
    });
    
});