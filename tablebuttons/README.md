TinyMCE table button control plugin, v1.0 (20.03.2015)
================

* ©2015 <http://www.win-soft.ch>
* Tested sucessfully with TinyMCE 4.1.7

Installation
----------------
* copy folder *tablebuttons* to TinyMCE's plugin directory
* add **tablebuttons** to plugins list (tinymce_plugins)
* add **tablebuttoncontrol** to toolbar list, where ever you want have table button control icon

